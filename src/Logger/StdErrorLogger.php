<?php

namespace Drupal\gu_logger\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LoggerInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Forward Drupal logs to stderr.
 *
 * @see https://www.drupal.org/docs/8/api/logging-api/overview
 */
class StdErrorLogger implements LoggerInterface {
  use RfcLoggerTrait;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(LogMessageParserInterface $parser) {
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []) {
    // Don't log if we have no namespace set.
    if (empty(getenv('APP_NAMESPACE'))) {
      return;
    }

    try {
      $severity = strtoupper(RfcLogLevel::getLevels()[$level]);
      $uid = $context['uid'];
      $variables = $this->parser->parseMessagePlaceholders($message, $context);
      $input_message = strip_tags(new FormattableMarkup($message, $variables));
      $input_message = str_replace(["\n", "\r"], '¶ ', $input_message);
      $application_namespace = getenv('APP_NAMESPACE');

      $formatted_message = new FormattableMarkup('@date | @deployment: [@severity] [@type] @message | user: @user | ip: @ip | uri: @uri | referer: @referer', [
        '@date' => date('Y-m-d H:i:s', $context['timestamp']),
        '@deployment' => $application_namespace,
        '@severity' => $severity,
        '@type' => $context['channel'],
        '@message' => $input_message,
        '@user' => $uid,
        '@ip' => $context['ip'],
        '@uri' => $context['request_uri'],
        '@referer' => $context['referer'],
      ]);
      if ($level <= RfcLogLevel::ERROR) {
        // EMERGENCY, ALERT, CRITICAL, ERROR.
        $output = fopen('php://stderr', 'wb');
      }
      else {
        // WARNING, NOTICE, INFO, DEBUG.
        $output = fopen('php://stdout', 'wb');
      }
      fwrite($output, strip_tags($formatted_message) . "\n");
      fclose($output);
    }
    catch (\Exception $e) {
      // Exception while logging, try to to continue instead of crashing.
      if (!empty($output)) {
        fclose($output);
      }
    }
  }

}
