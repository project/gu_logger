GU Logger
This module redirects PHP log messages to stdout or stderr. This is typically useful in Docker deployments.

Features:
=========

- The module concatenates multi-line log messages into a single line, which is useful when redirecting the logs to a reporting system, like the ELK stack.
- The module accepts an environment variable called APP_NAMESPACE, which allows separating logs per environment when deploying the module in a multi-environment setup.

